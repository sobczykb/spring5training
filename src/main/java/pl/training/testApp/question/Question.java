package pl.training.testApp.question;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.RequiredArgsConstructor;
import pl.training.testApp.Test.Test;
import pl.training.testApp.answerOption.AnswerOption;
import pl.training.testApp.categoryQuestion.CategoryQuestion;

import javax.persistence.*;
import java.util.List;


@Entity
@RequiredArgsConstructor
@AllArgsConstructor
@Data
public class Question {
    @GeneratedValue
    @Id
    private Long id;
    @ManyToOne
    @JoinColumn(name="testId")
    private Test test;
    private String content;
    private Long categoryQuestionId;
    @OneToMany(targetEntity=AnswerOption.class, mappedBy="question", fetch= FetchType.EAGER)
    private List<AnswerOption> answerOptionList;


}
