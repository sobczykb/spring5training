package pl.training.testApp.question;

import lombok.Data;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import pl.training.testApp.IService;
import pl.training.testApp.answerOption.AnswerOption;

import java.util.ArrayList;
import java.util.List;

@RequiredArgsConstructor
@Data
public class QuestionService implements IService<Question> {

    @NonNull
    QuestionRepository questionRepository;

    @Override
    public void create(Question question) throws QuestionAlreadyExistsException {
        if(questionRepository.findByContent(question.getContent()).isPresent())
            throw new QuestionAlreadyExistsException();
    }

    @Override
    public void init() {

        Question q = new Question();
        q.setContent("Czy aby na pewno chcesz wykonac ten test?");

        List<AnswerOption> options = new ArrayList<>();
        

    }

}