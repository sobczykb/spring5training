package pl.training.testApp.question;

import org.springframework.data.jpa.repository.JpaRepository;
import pl.training.testApp.categoryQuestion.CategoryQuestion;

import javax.swing.text.html.Option;
import java.util.Optional;

public interface QuestionRepository extends JpaRepository<Question,Long> {

    Optional<Question> findByContent(String content);
}
