package pl.training.testApp.categoryQuestion;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.RequiredArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Entity
@RequiredArgsConstructor
@AllArgsConstructor
@Data
public class CategoryQuestion {
    @GeneratedValue
    @Id
    private Long id;
    private String categoryName;


}
