package pl.training.testApp.categoryQuestion;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.RequiredArgsConstructor;
import pl.training.testApp.IService;

import java.util.ArrayList;
import java.util.List;


@AllArgsConstructor
@RequiredArgsConstructor
@Data
public class CategoryQuestionService implements IService<CategoryQuestion> {

    CategoryQuestionRepository categoryQuestionRepository;

    @Override
    public void create(CategoryQuestion categoryQuestion)  {
        if(categoryQuestionRepository.getByCategoryName(categoryQuestion.getCategoryName()).isPresent()){
            try {
                throw new CategoryAlreadyExixtsException();
            } catch (CategoryAlreadyExixtsException e) {
                e.printStackTrace();
            }
        }
        categoryQuestionRepository.save(categoryQuestion);
    }

    @Override
    public void init() {
        CategoryQuestion algorytmy = new CategoryQuestion(),
                programowanie = new CategoryQuestion(),
                wiedzaOgolna = new CategoryQuestion();

        algorytmy.setCategoryName("Algorytmy");
        programowanie.setCategoryName("Programowanie");
        wiedzaOgolna.setCategoryName("Wiedza Ogolna");

        List<CategoryQuestion> categories = new ArrayList<>();
        categories.add(algorytmy);
        categories.add(programowanie);
        categories.add(wiedzaOgolna);

        for(CategoryQuestion cq : categories ){
            create(cq);
        }
    }
}
