package pl.training.testApp.categoryQuestion;

import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface CategoryQuestionRepository extends JpaRepository<CategoryQuestion,Long> {

    Optional<CategoryQuestion> getByCategoryName(String categoryName);
}
