package pl.training.testApp.answerOption;

import org.springframework.data.jpa.repository.JpaRepository;


public interface AnswerOptionRepository extends JpaRepository<AnswerOption,Long> {
}
