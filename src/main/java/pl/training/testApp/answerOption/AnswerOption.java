package pl.training.testApp.answerOption;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.RequiredArgsConstructor;
import pl.training.testApp.answer.correctAnswer.CorrectAnswer;
import pl.training.testApp.question.Question;

import javax.persistence.*;


@Entity
@RequiredArgsConstructor
@AllArgsConstructor
@Data
public class AnswerOption {
    @GeneratedValue
    @Id
    private Long id;
    @ManyToOne
    @JoinColumn(name="questionId")
    private Question question;
    private String content;
    @OneToOne(mappedBy = "answerOption", cascade = CascadeType.ALL,
            fetch = FetchType.EAGER, optional = false)
    private CorrectAnswer correctAnswer;





}
