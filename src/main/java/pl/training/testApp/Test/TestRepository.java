package pl.training.testApp.Test;

import org.springframework.data.jpa.repository.JpaRepository;
import pl.training.testApp.question.Question;

import java.util.Optional;

public interface TestRepository extends JpaRepository<Test,Long> {

    Optional<Test> findByTestName(String testName);
}
