package pl.training.testApp.Test;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.RequiredArgsConstructor;
import pl.training.testApp.question.Question;

import javax.persistence.*;
import java.util.List;


@Entity
@RequiredArgsConstructor
@AllArgsConstructor
@Data
public class Test {

    @GeneratedValue
    @Id
    private Long id;
    private String testName;
    @OneToMany(targetEntity=Question.class, mappedBy="test", fetch= FetchType.EAGER)
    private List<Question> questionList;
}
