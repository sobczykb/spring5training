package pl.training.testApp.Test;

import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import pl.training.testApp.IService;

@RequiredArgsConstructor
public class TestService implements IService<Test> {

    @NonNull
    private TestRepository testRepository;

    @Override
    public void create(Test test)  {
        if(testRepository.findByTestName(test.getTestName()).isPresent())
            try {
                throw new TestAlreadyExistsException();
            } catch (TestAlreadyExistsException e) {
                e.printStackTrace();
            }
    }

    @Override
    public void init() {
        Test test=new Test();
        test.setId(new Long(1));
        test.setTestName("Test z matematyki");

        create(test);
    }


}