package pl.training.testApp.answer.account;


import lombok.Data;
import lombok.AllArgsConstructor;
import lombok.RequiredArgsConstructor;
import pl.training.testApp.answer.Answer;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

@Entity
@RequiredArgsConstructor
@AllArgsConstructor
@Data
@DiscriminatorValue("ACCOUNT_ANSWER")
public class AccountAnswer extends Answer {

    private Long userId;

}
