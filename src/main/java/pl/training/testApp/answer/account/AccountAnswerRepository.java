package pl.training.testApp.answer.account;

import org.springframework.data.jpa.repository.JpaRepository;

public interface AccountAnswerRepository extends JpaRepository<AccountAnswer,Long> {
}
