package pl.training.testApp.answer;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.RequiredArgsConstructor;
import pl.training.testApp.answerOption.AnswerOption;
import pl.training.testApp.question.Question;

import javax.persistence.*;

@Entity
@RequiredArgsConstructor
@AllArgsConstructor
@Data
@Inheritance(strategy = InheritanceType.TABLE_PER_CLASS)

public abstract class  Answer {
    @GeneratedValue
    @Id
    protected Long id;
    protected String content;
    @OneToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "answerOptionId")
    protected AnswerOption answerOption;



}
