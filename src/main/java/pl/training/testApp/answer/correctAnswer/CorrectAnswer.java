package pl.training.testApp.answer.correctAnswer;


import lombok.Data;
import lombok.RequiredArgsConstructor;
import pl.training.testApp.answer.Answer;
import pl.training.testApp.answerOption.AnswerOption;

import javax.persistence.*;

@Entity
@RequiredArgsConstructor
@Data
@DiscriminatorValue("CORRECT_ANSWER")
public class CorrectAnswer extends Answer {


}
