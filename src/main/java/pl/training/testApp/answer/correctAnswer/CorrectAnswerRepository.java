package pl.training.testApp.answer.correctAnswer;

import org.springframework.data.jpa.repository.JpaRepository;

public interface CorrectAnswerRepository extends JpaRepository<CorrectAnswer,Long> {
}
