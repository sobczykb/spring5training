package pl.training.testApp;

import pl.training.testApp.question.QuestionAlreadyExistsException;

public interface IService<T>{

   public void create(T var) throws QuestionAlreadyExistsException;

   public void init() ;
}
