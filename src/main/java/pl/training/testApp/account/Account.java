package pl.training.testApp.account;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.RequiredArgsConstructor;
import pl.training.testApp.account.accountTestResult.AccountTestResult;

import javax.persistence.*;
import java.util.List;

@Entity
@RequiredArgsConstructor
@AllArgsConstructor
@Data
public class Account {
    @GeneratedValue
    @Id
    private Long id;
    private String name;
    private String surname;
    @OneToMany(
            mappedBy = "account",
            cascade = CascadeType.ALL,
            orphanRemoval = true
    )
    private List<AccountTestResult> accountTestResultList;


}
