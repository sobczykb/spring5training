package pl.training.testApp.account.accountTestResult;

import org.springframework.data.jpa.repository.JpaRepository;

public interface AccountTestResultRepository extends JpaRepository<AccountTestResult,Long> {
}
