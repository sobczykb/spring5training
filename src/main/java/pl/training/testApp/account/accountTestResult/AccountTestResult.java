package pl.training.testApp.account.accountTestResult;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.RequiredArgsConstructor;
import pl.training.testApp.Test.Test;
import pl.training.testApp.account.Account;

import javax.persistence.*;

@Entity
@RequiredArgsConstructor
@AllArgsConstructor
@Data
public class AccountTestResult {
    @GeneratedValue
    @Id
    private Long id;
    @ManyToOne
    private Test test;
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "account_id")
    private Account account;
    private int result;
    private boolean pass;
}
